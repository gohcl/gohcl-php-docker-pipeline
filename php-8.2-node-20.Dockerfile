FROM php:8.2-cli

LABEL maintainer="Kurt Wolf"

RUN apt-get update && apt-get install -y \
    autoconf \
    bash \
    build-essential \
    curl \
    # freetype-dev \
    # gettext-dev \
    git \
    imagemagick \
    libfreetype-dev \
    # imagemagick-dev \
    # jpeg-dev \
    # libjpeg-turbo-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libmcrypt-dev \
    libonig-dev \
    libpng-dev \
    libpq-dev \
    libxml2-dev \
    libxslt-dev \
    libzip-dev \
    # linux-headers \
    # oniguruma-dev \
    # openssl \
    # openssl-dev \
    # pcre-dev \
    # postgresql-dev \
    wget
    # zlib-dev
    # && rm /var/cache/apk/*

RUN pecl channel-update pecl.php.net \
    && pecl install mcrypt redis-5.3.4 \
    && rm -rf /tmp/pear

RUN docker-php-ext-install \
    bcmath \
    exif \
    gettext \
    intl \
    mbstring \
    mysqli \
    pcntl \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    soap \
    sockets \
    xml \
    xsl \
    zip \
    && echo

RUN docker-php-ext-configure gd --with-freetype=/usr/lib/ --with-jpeg=/usr/lib/ \
    && docker-php-ext-install gd

RUN docker-php-ext-enable redis

# Install Composer
RUN cd /usr/bin \
    && curl -s https://getcomposer.org/installer | php \
    && ln -s /usr/bin/composer.phar /usr/bin/composer

# Install Node
ENV NODE_VERSION 20.11.1
ENV ARCH x64

RUN curl -fsSLO --compressed "https://nodejs.org/download/release/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
    && tar -xf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 \
    && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz"
