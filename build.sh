#!/bin/bash

slug="$1"
build="$2"
namespace="itguy614"

# Build the image
docker build . --platform linux/amd64 --file "$build.Dockerfile" -t "$namespace/$slug:$build"

# Save the image
# docker save "$namespace/$slug:$build" > "$slug-$build.tar"

# Upload the image to the registry
docker push "$namespace/$slug:$build"
