## DockerHub

- https://hub.docker.com/repository/docker/itguy614/gohcl-php-docker-pipeline/general

## Install and build your own Node install

- https://github.com/nodejs/docker-node/blob/main/14/alpine3.15/Dockerfile

## Increase build space

- https://community.atlassian.com/t5/Bitbucket-questions/Pipelines-Increase-the-Build-Container-Memory-above-1024mb/qaq-p/995713
- https://levelup.gitconnected.com/solving-bitbucket-pipelines-memory-issues-62c5a236ef96

## Vapor dockerfile example

- https://github.com/laravel/vapor-dockerfiles/blob/master/build.sh

## Copy prebuilt Node

- https://github.com/edbizarro/gitlab-ci-pipeline-php/tree/master/php/8.0/alpine
- https://github.com/edbizarro/gitlab-ci-pipeline-php/blob/master/php/scripts/alpine/nodeyarn.sh

## Alternate node build

- https://github.com/mhart/alpine-node/blob/1c1333be629f3fac3dabae8259efe699b012e254/build.dockerfile
